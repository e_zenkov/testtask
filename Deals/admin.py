from django.contrib import admin
from Deals.models import *

# Register your models here.

class DealAdmin(admin.ModelAdmin):
    list_display = ('date', 'name_insured')


class TypeDealAdmin(admin.ModelAdmin):
    list_display = ('id', 'type')


class ContactPersonAdmin(admin.ModelAdmin):
    list_display = ('id','contact_person')


class FilesAdmin(admin.ModelAdmin):
    list_display = ('id','name')
admin.site.register(TypeDeal, TypeDealAdmin)
admin.site.register(ContactPerson, ContactPersonAdmin)
admin.site.register(Deal, DealAdmin)
admin.site.register(Files,FilesAdmin)
