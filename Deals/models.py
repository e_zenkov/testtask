import datetime

from django.db import models

# Create your models here.


class Deal(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateField('Date', default=datetime.date.today())
    name_insured = models.CharField('Name of the Insured', max_length=100)
    obligor = models.CharField('Obligor', max_length=100)
    country = models.CharField('Country', max_length=100)
    ammount = models.FloatField('Amount, million')
    currency = models.CharField('Currency', max_length=100)
    percent = models.FloatField('% to be Insured')
    tenor = models.FloatField('Tenor in years')
    type_deal = models.CharField('TypeDeal', max_length=100)
    target_date = models.DateField('Target Response Date')
    contact_person = models.ForeignKey('ContactPerson')


class TypeDeal(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField('Type of Deal', max_length=100)


class ContactPerson(models.Model):
    id = models.AutoField(primary_key=True)
    contact_person = models.CharField('Name of the Contact Person', max_length=100)


class Files(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField('DateTime', default=datetime.datetime.utcnow())
    name = models.CharField('Name', max_length=100)
    link = models.FileField('Link', upload_to='media')
    deal_id = models.IntegerField('Deal Id')


class Comments(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField('DateTime', default=datetime.datetime.utcnow())
    deal_id = models.IntegerField('Deal Id')
    username = models.CharField('User name', max_length=100)
    comment = models.TextField('Comment')