import json

from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import modelform_factory
from django.db import transaction
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from Deals.models import *


@login_required
def index(request):
    return render(request, "index.html")


@transaction.atomic
@login_required
def ajax_get_all(request):
    if request.method == "GET" and request.is_ajax():
        data = Deal.objects.all()
        paginator = Paginator(data, 8)
        page = request.GET.get('page')
        try:
            list_deal = paginator.page(page)
        except PageNotAnInteger:
            list_deal = paginator.page(1)
        except EmptyPage:
            list_deal = paginator.page(paginator.num_pages)
        dict_data = {"success": "true", "total": paginator.count, 'data': []}
        for item in list_deal:
            dict_data["data"].append(
                {"name_insured": item.name_insured, "obligor": item.obligor, "country": item.country,
                 "date": datetime.date.strftime(item.date, "%d.%m.%Y"), "ammount": item.ammount,
                 "currency": item.currency, "percent": item.percent, "tenor": item.tenor, "type_deal": item.type_deal,
                 "target_date": datetime.date.strftime(item.target_date, "%d.%m.%Y"),
                 "contact_person": item.contact_person.contact_person, "id": item.id})
        return HttpResponse(json.dumps(dict_data), content_type='application/json')


@sensitive_post_parameters()
@never_cache
@csrf_protect
def login(request):
    if request.method == 'POST':
        if request.user.is_authenticated():
            data = {'success': 'true'}
            return HttpResponse(json.dumps(data), content_type="")
        username = request.POST['login']
        password = request.POST['pass']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            data = {'success': 'true'}
        else:
            data = {'failure': 'true'}
        return HttpResponse(json.dumps(data), content_type="")
    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect("/")
        return render(request, "accounts/login.html")


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")


@transaction.atomic
@login_required
@csrf_exempt
def upload(request):
    if request.method == "POST":
        form = modelform_factory(Files, fields=("name", "link", "deal_id"))
        form = form(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            data = {'success': 'true'}
        else:
            data = {'failure': 'true'}
        return HttpResponse(json.dumps(data), content_type="")
    id_deal = int(request.GET['id'])
    data = Files.objects.all().filter(deal_id=id_deal)
    dict_data = {"success": "true", "data": []}
    for record in data:
        dict_data["data"].append(
            {"date": record.date.strftime('%d.%m.%Y %H:%M'),
             "name": "<a href= %s > %s </a>" % (record.link, record.name)})
    return HttpResponse(json.dumps(dict_data), content_type='application/json')


@transaction.atomic
@login_required
def add_form(request):
    if request.method == "POST" and request.is_ajax():
        Deal.objects.create(
            date=datetime.datetime.strptime(request.POST['date'], '%d.%m.%Y'),
            name_insured=request.POST['name_insured'],
            obligor=request.POST['obligor'],
            country=request.POST['country'],
            ammount=float(request.POST['ammount']),
            currency=request.POST['currency'],
            percent=float(request.POST['percent']),
            tenor=request.POST['tenor'],
            type_deal=request.POST['type_deal'],
            target_date=datetime.datetime.strptime(request.POST['target_date'], '%d.%m.%Y'),
            contact_person=ContactPerson.objects.get(pk=int(request.POST['contact_person'])))
        data = {'success': 'true'}
        return HttpResponse(json.dumps(data), content_type="")
    return render(request, "add_form.html")


@transaction.atomic
@login_required
def get_persons(request):
    if not (request.method == "GET" and request.is_ajax()):
        return
    data = ContactPerson.objects.all()
    dict_data = {"success": "true", "persons": []}
    for record in data:
        dict_data["persons"].append({"contact_person": record.contact_person, "Id": record.id})
    return HttpResponse(json.dumps(dict_data), content_type='application/json')


@transaction.atomic
@login_required
def comment(request):
    if not (request.is_ajax()):
        return
    if request.method == "POST":
        Comments.objects.create(
            deal_id=int(request.POST['id']),
            comment=request.POST['comment'],
            username=request.user
        )
        data = {'success': 'true'}
        return HttpResponse(json.dumps(data), content_type="")
    id_deal = int(request.GET['id'])
    data = Comments.objects.all().filter(deal_id=id_deal)
    dict_data = {"success": "true", "data": []}
    for record in data:
        dict_data['data'].append(
          {"datetime": record.date.strftime('%d.%m.%Y %H:%M'), "user": record.username, "comment": record.comment})
    return HttpResponse(json.dumps(dict_data), content_type='application/json')