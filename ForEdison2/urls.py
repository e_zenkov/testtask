from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
                       url(r'^accounts/login', 'Deals.views.login'),
                       url(r'^accounts/logout', 'Deals.views.logout'),
                       url(r'^admin', include(admin.site.urls)),
                       url(r'^$', "Deals.views.index", name='index'),
                       url(r'^getdata', "Deals.views.ajax_get_all", name='index'),
                       url(r'^data/adddata', "Deals.views.add_form", name='index'),
                       url(r'^data/get_persons', "Deals.views.get_persons"),
                       url(r'^data/upload', "Deals.views.upload"),
                       url(r'^Comment', "Deals.views.comment"),
                       url(r'^test', "Deals.views.test"),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT+'/media'})
)
