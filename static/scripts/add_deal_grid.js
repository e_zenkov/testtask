/**
 * Created by zenkov on 21.11.2014.
 */
Ext.require([
        'Ext.util.Cookies'
    ]);
Ext.require('Ext.form.*');

Ext.onReady(function() {
    Ext.define('MyApp.FileFieldAttributes', {
        override: 'Ext.form.field.File',
        xtype: 'filefield',
        fileInputAttributes: {
            accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        },
        onRender: ( function () {
            var me = this,
                attr = me.fileInputAttributes,
                fileInputEl, name;

            me.callParent();
            fileInputEl = me.getTrigger('filebutton').component.fileInputEl.dom;
            for (name in attr) {
                fileInputEl.setAttribute(name, attr[name]);
            }
        })
    });


    Ext.apply(Ext.form.field.VTypes, {
        file: function (val, field) {
            var types = ['doc', 'docx', 'xls', 'xlsx', 'pdf', 'jpg'],
                ext = val.substring(val.lastIndexOf('.') + 1);
            if (Ext.Array.indexOf(types, ext) === -1) {
                return false;
            }
        }, fileText: 'Invalid file'
    });
    Ext.Ajax.setDefaultHeaders({
        'X-CSRFToken': Ext.util.Cookies.get('csrftoken')
    });

    Ext.define('Persons', {
        extend: 'Ext.data.Model',
        fields: [
            {type: 'string', name: 'contact_person'},
            {type: 'int', name: 'Id'}]
    });
    var store = Ext.create('Ext.data.Store', {
        model: 'Persons',
        proxy: {
            type: 'ajax',
            url: '/data/get_persons',
            reader: {
                type: 'json',
                root: 'persons'
            }
        }
    });
    store.load();
    var loginForm = Ext.create('Ext.form.Panel', {
        renderTo: Ext.getBody(),
        title: 'add Form',
        height: 600,
        width: 500,
        bodyPadding: 10,
        items: [
            {
                xtype: 'datefield', allowBlank: false, name: 'date', format: 'd.m.Y',
                fieldLabel: 'Date'
            },
            {
                xtype: 'textfield', name: 'name_insured',
                fieldLabel: 'Name of the Insured', allowBlank: false, minLength: 3, maxLength: 50
            },
            {
                xtype: 'textfield', allowBlank: false, name: 'obligor',
                fieldLabel: 'Obligor'
            },
            {
                xtype: 'textfield', allowBlank: false, name: 'country',
                fieldLabel: 'Country'
            },
            {
                xtype: 'numberfield', allowBlank: false, name: 'ammount',
                fieldLabel: 'Amount, million'
            },
            {
                xtype: 'textfield', allowBlank: false, name: 'currency',
                fieldLabel: 'Currency'
            },
            {
                xtype: 'numberfield', allowBlank: false, name: 'percent',
                fieldLabel: '% to be Insured'
            },
            {
                xtype: 'numberfield', allowBlank: false, name: 'tenor',
                fieldLabel: 'Tenor in years'
            },
            {
                xtype: 'textfield', allowBlank: false, name: 'type_deal',
                fieldLabel: 'TypeDeal'
            },
            {
                xtype: 'datefield', allowBlank: false, name: 'target_date', format: 'd.m.Y',
                fieldLabel: 'Target Response Date'
            },
            {
                xtype: 'combobox', allowBlank: false, name: 'contact_person',
                fieldLabel: 'ContactPerson',
                autoSelect: true,
                store: store,
                valueField: 'Id',
                displayField: 'contact_person',
                queryMode: 'local',
                blankText: 'Select person',
                editable: false
            }
            //   {
            //       xtype:'filefield',
            //       vtype:'file',
            //       name:'filef',
            //       fieldLabel:'File',
            //       acceptMimes: ['doc','docx', 'xls', 'xlsx', 'pdf', 'jpg']
            // extensions:['doc','docx', 'xls', 'xlsx', 'pdf', 'jpg']
            //   }
        ],
        buttons: [
            {
                text: 'Save',
                handler: function () {
                    if (loginForm.isValid()) {
                        loginForm.getForm().submit({
                            url: '/data/adddata',
                            success: function (form, action) {
                                Ext.MessageBox.alert('Success', 'Deal saved ');
                                document.location = ('/');
                            },
                            failure: function (form, action) {
                                Ext.MessageBox.alert('Error');
                            }
                        });
                    }
                    else {
                        Ext.MessageBox.alert('Error', 'Fill all fields');
                    }
                }
            },
            {
                text: 'Close',
                handler: function () {
                    {
                        document.location = ('/');
                    }
                }
            }
        ]
    });
});
