/**
 * Created by zenkov on 19.11.2014.
 */

Ext.require([
        'Ext.util.Cookies'
    ]);
Ext.require('Ext.form.*');
Ext.onReady(function() {
    Ext.Ajax.setDefaultHeaders({
                                   'X-CSRFToken': Ext.util.Cookies.get('csrftoken')
                               });
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    var loginForm = Ext.create('Ext.form.Panel', {
        ///renderTo: Ext.getBody(),
        height: 150,
        width: 300,
        bodyPadding: 10,
        defaultType: 'textfield',
        items: [
            {
                fieldLabel: 'Login',
                name: 'login',
                allowBlank: false
            },
            {
                fieldLabel: 'Password', allowBlank: false,
                name: 'pass',
                inputType: 'password'
            }
        ],
        buttons: [
            {
                text: 'Send',
                handler: function () {
                    if (!loginForm.isValid()) {
                        Ext.MessageBox.alert('Error','Enter your login and password');
                        return;
                    }
                    loginForm.getForm().submit({
                                                   url: '/accounts/login',
                                                   success: function (form, action) {
                                                       Ext.MessageBox.alert('Authorize successful');
                                                       document.location = ('/');
                                                   },
                                                   failure: function (form, action) {
                                                       Ext.MessageBox.alert('Authorize error', 'Username or password \i' +
                                                       'ncorrect. Please re enter your login and password');
                                                   }
                                               });
                }
            }
        ]
    });
    var window = new Ext.Window({
                                    title: 'Please login',
                                    layout: 'fit',
                                    width: 400,
                                    height: 150,
                                    closable: false,
                                    resizable: false,
                                    plain: false,
                                    items: [loginForm]
                                });
    window.show();
});