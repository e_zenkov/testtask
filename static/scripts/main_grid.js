var itemsPerPage = 8;
Ext.onReady(function() {
    Ext.require([
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.panel.*',
        'Ext.layout.container.Border'
    ]);
    Ext.Ajax.setDefaultHeaders({
        'X-CSRFToken': Ext.util.Cookies.get('csrftoken')
    });
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.define('Files', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'ajax',
            reader: 'json'
        },
        fields: [
            {
                name: 'date', type: 'string',
                name: 'name', type: 'string'
            }
        ]
    });
    Ext.define('Comments', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'ajax',
            reader: 'json'
        },
        fields: [
            {
                name: 'datetime', type: 'string',
                name: 'user', type: 'string',
                name: 'comment', type: 'string'
            }
        ]
    });
    Ext.define('Book', {
        extend: 'Ext.data.Model',
        proxy: {
            type: 'ajax',
            reader: 'xml'
        },
        fields: [
            {
                name: 'id', type: 'int',
                name: 'date', type: 'date', dateFormat: 'd.m.Y',
                name: 'name_insured', type: 'string',
                name: 'obligor', type: 'string',
                name: 'country', type: 'string',
                name: 'ammount', type: 'float',
                name: 'currency', type: 'string',
                name: 'percent', type: 'float',
                name: 'tenor', type: 'float',
                name: 'type_deal', type: 'string',
                name: 'target_date', type: 'date', dateFormat: 'd.m.Y',
                name: 'contact_person', type: 'string'
            }
        ]
    });

    // create the Data Store
    var store = Ext.create('Ext.data.Store', {
        model: 'Book',

        pageSize: 8,
        proxy: {
            // load using HTTP
            type: 'ajax',
            url: '/getdata/',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    });
    // create the grid
    var grid = Ext.create('Ext.grid.Panel', {
        bufferedRenderer: false,
        autoScroll: true,
        store: store,
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: store,
            dock: 'bottom',
            displayInfo: true,
            beforePageText: 'Page',
            afterPageText: 'of {0}',
            displayMsg: 'Deals {0} - {1} of {2}'
        }],
        columns: [
            {text: "Date", flex: 0.5, dataIndex: 'date', type: 'date', dateFormat: 'd.m.Y', sortable: true},
            {text: "Name of <br />the insured", flex: 1, dataIndex: 'name_insured', sortable: true},
            {text: "Name of <br />the obligor", flex: 1, dataIndex: 'obligor', sortable: true},
            {text: "Country of <br />the obligor", flex: 1, dataIndex: 'country', sortable: true},
            {text: "Amount", flex: 0.4, dataIndex: 'ammount', sortable: true},
            {text: "Currency", flex: 0.4, dataIndex: 'currency', sortable: true},
            {text: "% to be<br />insured", flex: 0.4, dataIndex: 'percent', sortable: true},
            {text: "Tenor<br /> in year", flex: 0.4, dataIndex: 'tenor', sortable: true},
            {text: "Type of Deal", flex: 1, dataIndex: 'type_deal', sortable: true},
            {
                text: "Target<br /> Response Date",
                flex: 0.6,
                dataIndex: 'target_date',
                sortable: true,
                dateFormat: 'd.m.Y'
            },
            {text: "Name of <br />the Contact Person", flex: 1, dataIndex: 'contact_person', sortable: true},
        ],
        forceFit: true,
        height: 300,
        width: '100%',
        split: true,
        region: 'north'
    });

    // define a template to use for the detail view
    var bookTplMarkup = [
        '<Table><TABLE BORDER=1><TR>',
        '<TD>Date</TD><TD>{date}</TD></TR><TR>',
        '<TD>Name of the Insured</TD><TD> {name_insured}</TD></TR><TR>',
        '<TD>Obligor:</TD><TD> {obligor}</TD></TR><TR>',
        '<TD>Country:</TD><TD> {country}</TD></TR><TR>',
        '<TD>Amount, million:</TD><TD> {ammount}</TD></TR><TR>',
        '<TD>Currency:</TD><TD> {currency}</TD></TR><TR>',
        '<TD>% to be Insured:</TD><TD> {percent}</TD></TR><TR>',
        '<TD>Tenor in years</TD><TD> {tenor}</TD></TR><TR>',
        '<TD>Type of Deal</TD><TD> {type_deal}</TD></TR><TR>',
        '<TD>Target Response Date</TD><TD> {target_date}</TD></TR><TR>',
        '<TD>Name of the Contact Person</TD><TD> {contact_person}</TD>',
        '</TR></TABLE>'
    ];
    var bookTpl = Ext.create('Ext.Template', bookTplMarkup);
    var storeComment = Ext.create('Ext.data.Store', {
        model: 'Comments',
        proxy: {
            type: 'ajax',
            url: '/Comment',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    });
    var buttPanel = Ext.create('Ext.Panel', {
        frame: true,
        title: '',
        height: 45,
        region: 'north',
        buttons: [{
            text: 'View more info',
            handler: function () {
                if (grid.getSelection().length) {
                    mainPanel.setVisible(false);
                    detailPanel.setVisible(true);
                    storeComment.load({
                        params: {
                            id: fullData['id']
                        }
                    });
                    storeFiles.load({
                        params: {
                            id: fullData['id']
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert('Error', 'Select deal first');
                }
            }
        },
            {
                text: 'Add new deal',
                handler: function () {
                    document.location = ('/data/adddata');
                }
            }]
    });
    var mainPanel = Ext.create('Ext.Panel', {
        renderTo: Ext.getBody(),
        frame: true,
        title: 'List of deals',
        width: '100%',
        height: 900,
        layout: 'border',
        items: [
            grid, buttPanel
        ]
    });
    var fullData = null;
    grid.getSelectionModel().on('selectionchange', function (sm, selectedRecord) {
        if (selectedRecord.length) {
            fullData = selectedRecord[0].data;
            var detailPanel = Ext.getCmp('detailPanel');
            detailPanel.update(bookTpl.apply(selectedRecord[0].data));

        }
    });

    var commentForm = Ext.create('Ext.form.Panel', {
        name: 'commPanel',
        region: 'south',
        height: 100,
        width: '100%',
        items: [
            {
                region: 'west',
                xtype: 'textareafield',
                name: 'commentfield',
                id: 'commentfield',
                fieldLabel: 'Your comment',
                width: '100%',
                height: 50, allowBlank: false, minLength: 3
            }, {
                region: 'center',
                xtype: 'button',
                name: 'comment',
                text: 'send',
                handler: function () {
                    if (!commentForm.isValid()) {
                        Ext.MessageBox.alert('Error', 'Enter your comment');
                        return;
                    }
                    var com = Ext.getCmp('commentfield').value;
                    Ext.Ajax.request({
                            url: "/Comment",
                            success: function (response, opts) {
                                Ext.getCmp('commentfield').reset();
                                storeComment.load({
                                    params: {
                                        id: fullData['id']
                                    }
                                });
                            },
                            method: "POST",
                            params: ({
                                comment: com,
                                id: fullData['id']
                            }),

                            failure: function (response, opts) {
                                Ext.MessageBox.alert('Error', 'server-side failure with status code ' + response.status);
                            }
                        }
                    );
                }
            }]
    });
    var commentGrid = Ext.create('Ext.grid.Panel', {
        bufferedRenderer: false,
        autoScroll: true,
        store: storeComment,
        columns: [
            {text: "", width: 150, dataIndex: 'datetime'},
            {text: "", width: 150, dataIndex: 'user'},
            {text: "", flex: 1, dataIndex: 'comment'}
        ],
        forceFit: true,
        height: 250,
        width: '100%',
        split: true,
        region: 'north'
    });

    var commentPanel = Ext.create('Ext.Panel', {
        frame: true,
        title: 'Comments',
        width: '100%',
        height: 400,
        layout: 'border',
        region: 'south',
        items: [commentGrid, commentForm]
    });
    var buttonpanel = Ext.create('Ext.Panel', {
        region: 'east',
        width: 100,
        height: 100,
        layout: 'border',
        items: [
            {
                region: 'north',
                xtype: 'button',
                name: 'close',
                text: 'Close',
                handler: function () {
                    if (grid.getSelection().length) {
                        detailPanel.setVisible(false);
                        mainPanel.setVisible(true);
                    }
                }
            }, {
                region: 'north',
                xtype: 'button',
                name: 'addFile',
                text: 'Add file',
                handler: function () {
                    Ext.getCmp('file_id_deal').setValue(parseInt((fullData['id']), 10));
                    Ext.getCmp('uploadWindow').show();
                }
            }
        ]
    });
    wind = Ext.getCmp('uploadWindow');
    wind.on('beforeclose', function () {
        storeFiles.load({
            params: {
                id: fullData['id']
            }
        });
    });
    var leftPanel = Ext.create('Ext.Panel', {
        region: 'west',
        width: 500,
        height: 350,
        layout: 'border',
        items: [
            {
                id: 'detailPanel',
                region: 'center',
                bodyPadding: 7,
                bodyStyle: "background: #ffffff;"
            }]
    });
    var storeFiles = Ext.create('Ext.data.Store', {
        id: 'storeFiles',
        model: 'Files',
        proxy: {
            type: 'ajax',
            url: '/data/upload',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    });
    var fileGrid = Ext.create('Ext.grid.Panel', {
        bufferedRenderer: false,
        autoScroll: true,
        store: storeFiles,
        columns: [
            {text: "Date of upload", flex: 0.5, dataIndex: 'date', type: 'date', dateFormat: 'd.m.Y', sortable: true},
            {text: "Name of file", flex: 1, dataIndex: 'name', sortable: true},

        ],
        forceFit: true,
        height: 350,
        width: '100%',
        split: true,
        region: 'center'
    });
    var detailPanel = Ext.create('Ext.Panel', {
        renderTo: Ext.getBody(),
        frame: true,
        title: 'Information',
        width: '100%',
        height: 800,
        layout: 'border',
        items: [
            leftPanel, fileGrid, buttonpanel, commentPanel
        ]
    });

    detailPanel.setVisible(false);
    store.load({
        params: {
            start: 0,
            limit: itemsPerPage
        }
    });

});