/**
 * Created by zenkov on 19.11.2014.
 */

Ext.require([
        'Ext.util.Cookies'
    ]);
Ext.require([
    'Ext.window.Window',
    'Ext.tab.*',
    'Ext.toolbar.Spacer',
    'Ext.layout.container.Card',
    'Ext.layout.container.Border',
    'Ext.panel.*',
    'Ext.form.*'
]);

/**
* vtype
*/
Ext.apply(Ext.form.field.VTypes, {
    file: function(val, field) {
        var types = ['doc', 'xls', 'xlsx', 'docx', 'pdf', 'jpeg', 'jpg'],
            ext = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
        return!(Ext.Array.indexOf(types, ext) === -1)
    }
    ,fileText: 'Invalid file extension'
});

Ext.onReady(function() {
    Ext.Ajax.setDefaultHeaders({
        'X-CSRFToken': Ext.util.Cookies.get('csrftoken')
    });
    var uploadForm = Ext.create('Ext.form.Panel', {
        bodyBorder: false,
        border: false,
        frame: false,
        height: '100%',
        width: '100%',
        items: [{
            xtype: 'filefield',
            vtype: 'file',
            msgTarget: 'under',
            width: '100%',
            fieldLabel: 'File',
            name: 'link',
            allowBlank: false
        },
            {
                xtype: 'textfield',
                width: '100%',
                name: 'name',
                fieldLabel: 'File title',
                allowBlank: false,
                region: 'south'
            },
            {
                xtype: 'numberfield',
                name: 'deal_id',
                allowBlank: false,
                id: 'file_id_deal'
            }
        ],
        buttons: [
            {
                text: 'Send',
                handler: function () {
                    if (uploadForm.isValid()) {
                        uploadForm.getForm().submit({
                            url: '/data/upload',
                            success: function (form, action) {
                                Ext.MessageBox.alert('successful');
                                uploadWindow.close();
                            },
                            failure: function (form, action) {
                                Ext.MessageBox.alert('error');
                            }
                        });
                    }
                }
            }
        ]

    });
    var uploadWindow = Ext.create('Ext.window.Window', {
        id: 'uploadWindow',
        height: 180,
        width: 400,
        title: 'Upload file  (only doc, docx, xls, xlsx, pdf, jpeg)',
        closable: true,
        plain: true,
        resizable: false,
        modal: true,
        //     frame : true,
        //      fit : true,
        closeAction: 'hide',
        //       title: 'User Form',
        //       height: 150,
        //       width: 300,
        bodyPadding: 10,
        items: [uploadForm]

    });

    Ext.getCmp('file_id_deal').setVisible(false);
});